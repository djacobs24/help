ENV_PKG = env/
ERRS_PKG = errs/
PAGE_PKG = paginate/
RAND_PKG = rand/
VALIDATE_PKG = validate/

LINT_CMD = golangci-lint run
TEST_CMD = go test -v -cover ./...
TIDY_CMD = go mod tidy

# Lint everything
.PHONY: lint
lint: lint-env lint-errs lint-paginate lint-rand lint-validate

# Test everything
.PHONY: test
test: test-env test-errs test-paginate test-rand test-validate

# Tidy everything
.PHONY: tidy
tidy: tidy-env tidy-errs tidy-paginate tidy-rand tidy-validate

# Lint only the env package
.PHONY: lint-env
lint-env:
	cd $(ENV_PKG) && $(LINT_CMD)

# Lint only the errs package
.PHONY: lint-errs
lint-errs:
	cd $(ERRS_PKG) && $(LINT_CMD)

# Lint only the paginate package
.PHONY: lint-paginate
lint-paginate:
	cd $(PAGE_PKG) && $(LINT_CMD)

# Lint only the rand package
.PHONY: lint-rand
lint-rand:
	cd $(RAND_PKG) && $(LINT_CMD)

# Lint only the validate package
.PHONY: lint-validate
lint-validate:
	cd $(VALIDATE_PKG) && $(LINT_CMD)

# Test only the env package
.PHONY: test-env
test-env:
	cd $(ENV_PKG) && $(TEST_CMD)

# Test only the errs package
.PHONY: test-errs
test-errs:
	cd $(ERRS_PKG) && $(TEST_CMD)

# Test only the paginate package
.PHONY: test-paginate
test-paginate:
	cd $(PAGE_PKG) && $(TEST_CMD)

# Test only the rand package
.PHONY: test-rand
test-rand:
	cd $(RAND_PKG) && $(TEST_CMD)

# Test only the validate package
.PHONY: test-validate
test-validate:
	cd $(VALIDATE_PKG) && $(TEST_CMD)

# Tidy only the env package
.PHONY: tidy-env
tidy-env:
	cd $(ENV_PKG) && $(TIDY_CMD)

# Tidy only the errs package
.PHONY: tidy-errs
tidy-errs:
	cd $(ERRS_PKG) && $(TIDY_CMD)

# Tidy only the paginate package
.PHONY: tidy-paginate
tidy-paginate:
	cd $(PAGE_PKG) && $(TIDY_CMD)

# Tidy only the rand package
.PHONY: tidy-rand
tidy-rand:
	cd $(RAND_PKG) && $(TIDY_CMD)

# Tidy only the validate package
.PHONY: tidy-validate
tidy-validate:
	cd $(VALIDATE_PKG) && $(TIDY_CMD)
package rand_test

import (
	"regexp"
	"testing"

	"gitlab.com/djacobs24/help/rand"
)

func TestString(t *testing.T) {
	type args struct {
		len int
	}
	tests := []struct {
		name    string
		args    args
		wantLen int
	}{
		{
			name: "returns correct len",
			args: args{
				len: 5,
			},
			wantLen: 5,
		},
		{
			name: "returns correct len - 0",
			args: args{
				len: 0,
			},
			wantLen: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := rand.String(tt.args.len); len(got) != tt.wantLen {
				t.Errorf("String() = got len %v, want len %v", len(got), tt.wantLen)
			}
		})
	}
}

func TestRegexWithLowerCharsResult(t *testing.T) {
	for i := 0; i < 10; i++ {
		gen := rand.String(100, rand.WithLowerChars())
		re, _ := regexp.Compile(`[a-z]`)
		if !re.MatchString(gen) {
			t.Errorf("invalid lower char set: generated string %q", gen)
		}
	}
}

func TestRegexWithUpperCharsResult(t *testing.T) {
	for i := 0; i < 10; i++ {
		gen := rand.String(100, rand.WithUpperChars())
		re, _ := regexp.Compile(`[A-Z]`)
		if !re.MatchString(gen) {
			t.Errorf("invalid upper char set: generated string %q", gen)
		}
	}
}

func TestRegexWithNumericCharsResult(t *testing.T) {
	for i := 0; i < 10; i++ {
		gen := rand.String(100, rand.WithNumericChars())
		re, _ := regexp.Compile(`[0-9]`)
		if !re.MatchString(gen) {
			t.Errorf("invalid numeric char set: generated string %q", gen)
		}
	}
}

func TestRegexWithNoOptionsResult(t *testing.T) {
	for i := 0; i < 10; i++ {
		gen := rand.String(100)
		re, _ := regexp.Compile(`[a-z]|[A-Z]|[0-9]`)
		if !re.MatchString(gen) {
			t.Errorf("invalid char set: generated string %q", gen)
		}
	}
}

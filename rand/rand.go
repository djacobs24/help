package rand

import (
	"math/rand"
	"time"
)

const (
	lowerCharSet   = "abcdefghijklmnopqrstuvwxyz"
	upperCharSet   = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	numericCharSet = "0123456789"
)

var seededRand = rand.New(rand.NewSource(time.Now().UnixNano()))

// String generates a random string of the provided length.
// If no options are provided, all character sets are applied.
func String(length int, options ...option) string {
	if length <= 0 {
		return ""
	}

	var charSet string
	for _, option := range options {
		charSet += option()
	}

	if len(charSet) == 0 {
		// No options were provided, default to all characters.
		charSet = lowerCharSet + upperCharSet + numericCharSet
	}

	b := make([]byte, length)
	for i := range b {
		b[i] = charSet[seededRand.Intn(len(charSet))]
	}

	return string(b)
}

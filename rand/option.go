package rand

type option func() string

// WithLowerChars applies lowercase characters
// to the character set.
func WithLowerChars() option {
	return func() string {
		return lowerCharSet
	}
}

// WithUpperChars applies uppercase characters
// to the character set.
func WithUpperChars() option {
	return func() string {
		return upperCharSet
	}
}

// WithNumericChars applies numeric characters
// to the character set.
func WithNumericChars() option {
	return func() string {
		return numericCharSet
	}
}

// WithCustomChars applies custom characters
// to the character set.
func WithCustomChars(chars string) option {
	return func() string {
		return chars
	}
}

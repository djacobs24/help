package rand_test

import (
	"reflect"
	"testing"

	"gitlab.com/djacobs24/help/rand"
)

func TestWithLowerChars(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		{
			name: "returns correct char set",
			want: "abcdefghijklmnopqrstuvwxyz",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := rand.WithLowerChars()(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("WithLower() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestWithUpperChars(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		{
			name: "returns correct char set",
			want: "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := rand.WithUpperChars()(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("WithUpper() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestWithNumericChars(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		{
			name: "returns correct char set",
			want: "0123456789",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := rand.WithNumericChars()(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("WithNumeric() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestWithCustomChars(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		{
			name: "returns correct char set",
			want: "#*&(",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := rand.WithCustomChars("#*&(")(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("WithNumeric() = %v, want %v", got, tt.want)
			}
		})
	}
}

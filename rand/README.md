# rand

Random string generation

## Usage

```golang
package main

import (
    "gitlab.com/djacobs24/help/rand"
)


func main() {
    id := rand.String(
        32,
        rand.WithLowerChars(), 
        rand.WithNumericChars(), 
        rand.WithCustomChars("_@#$*"),
    )
}

```
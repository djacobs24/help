package errs

import (
	"errors"
	"fmt"
	"net/http"
	"reflect"
	"testing"
)

func TestNewInvalidArgument(t *testing.T) {
	type args struct {
		options []errOpt
	}
	tests := []struct {
		name string
		args args
		want Error
	}{
		{
			name: "no options, generic message only",
			args: args{
				options: []errOpt{},
			},
			want: Error{
				typ:    TypeInvalidArgument,
				pretty: false,
				genMsg: TypeInvalidArgument.String(),
				msg:    "",
			},
		},
		{
			name: "msg, not pretty",
			args: args{
				options: []errOpt{
					WithMsg("name is required"),
				},
			},
			want: Error{
				typ:    TypeInvalidArgument,
				pretty: false,
				genMsg: TypeInvalidArgument.String(),
				msg:    "name is required",
			},
		},
		{
			name: "msgf, not pretty",
			args: args{
				options: []errOpt{
					WithMsgf("error validating: %s", "name is required"),
				},
			},
			want: Error{
				typ:    TypeInvalidArgument,
				pretty: false,
				genMsg: TypeInvalidArgument.String(),
				msg:    "error validating: name is required",
			},
		},
		{
			name: "err, not pretty",
			args: args{
				options: []errOpt{
					WithErr(errors.New("name is required")),
				},
			},
			want: Error{
				typ:    TypeInvalidArgument,
				pretty: false,
				genMsg: TypeInvalidArgument.String(),
				msg:    "name is required",
			},
		},
		{
			name: "err, pretty",
			args: args{
				options: []errOpt{
					WithPrettyErr(errors.New("name is required")),
				},
			},
			want: Error{
				typ:    TypeInvalidArgument,
				pretty: true,
				genMsg: TypeInvalidArgument.String(),
				msg:    "name is required",
			},
		},
		{
			name: "msg, pretty",
			args: args{
				options: []errOpt{
					WithPrettyMsg("name is required"),
				},
			},
			want: Error{
				typ:    TypeInvalidArgument,
				pretty: true,
				genMsg: TypeInvalidArgument.String(),
				msg:    "name is required",
			},
		},
		{
			name: "msgf, pretty",
			args: args{
				options: []errOpt{
					WithPrettyMsgf("error validating: %s", "name is required"),
				},
			},
			want: Error{
				typ:    TypeInvalidArgument,
				pretty: true,
				genMsg: TypeInvalidArgument.String(),
				msg:    "error validating: name is required",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewInvalidArgument(tt.args.options...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewInvalidArgument() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewNotFound(t *testing.T) {
	type args struct {
		options []errOpt
	}
	tests := []struct {
		name string
		args args
		want Error
	}{
		{
			name: "no options, generic message only",
			args: args{
				options: []errOpt{},
			},
			want: Error{
				typ:    TypeNotFound,
				pretty: false,
				genMsg: TypeNotFound.String(),
				msg:    "",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewNotFound(tt.args.options...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewNotFound() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewAlreadyExists(t *testing.T) {
	type args struct {
		options []errOpt
	}
	tests := []struct {
		name string
		args args
		want Error
	}{
		{
			name: "no options, generic message only",
			args: args{
				options: []errOpt{},
			},
			want: Error{
				typ:    TypeAlreadyExists,
				pretty: false,
				genMsg: TypeAlreadyExists.String(),
				msg:    "",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewAlreadyExists(tt.args.options...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewAlreadyExists() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewPermissionDenied(t *testing.T) {
	type args struct {
		options []errOpt
	}
	tests := []struct {
		name string
		args args
		want Error
	}{
		{
			name: "no options, generic message only",
			args: args{
				options: []errOpt{},
			},
			want: Error{
				typ:    TypePermissionDenied,
				pretty: false,
				genMsg: TypePermissionDenied.String(),
				msg:    "",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewPermissionDenied(tt.args.options...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewPermissionDenied() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewUnauthenticated(t *testing.T) {
	type args struct {
		options []errOpt
	}
	tests := []struct {
		name string
		args args
		want Error
	}{
		{
			name: "no options, generic message only",
			args: args{
				options: []errOpt{},
			},
			want: Error{
				typ:    TypeUnauthenticated,
				pretty: false,
				genMsg: TypeUnauthenticated.String(),
				msg:    "",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewUnauthenticated(tt.args.options...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewUnauthenticated() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewUnimplemented(t *testing.T) {
	type args struct {
		options []errOpt
	}
	tests := []struct {
		name string
		args args
		want Error
	}{
		{
			name: "no options, generic message only",
			args: args{
				options: []errOpt{},
			},
			want: Error{
				typ:    TypeUnimplemented,
				pretty: false,
				genMsg: TypeUnimplemented.String(),
				msg:    "",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewUnimplemented(tt.args.options...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewUnimplemented() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewInternal(t *testing.T) {
	type args struct {
		options []errOpt
	}
	tests := []struct {
		name string
		args args
		want Error
	}{
		{
			name: "no options, generic message only",
			args: args{
				options: []errOpt{},
			},
			want: Error{
				typ:    TypeInternal,
				pretty: false,
				genMsg: TypeInternal.String(),
				msg:    "",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewInternal(tt.args.options...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewInternal() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestError_Marshal(t *testing.T) {
	type fields struct {
		typ    errType
		pretty bool
		genMsg string
		msg    string
	}
	tests := []struct {
		name   string
		fields fields
		want   []byte
	}{
		{
			name: "marshals correct fields",
			fields: fields{
				typ:    "",
				pretty: false,
				genMsg: TypeInvalidArgument.String(),
				msg:    "name is a required field",
			},
			want: []byte(fmt.Sprintf(`{"pretty":false,"generic_message":"%s","message":"name is a required field"}`, TypeInvalidArgument.String())),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := Error{
				typ:    tt.fields.typ,
				pretty: tt.fields.pretty,
				genMsg: tt.fields.genMsg,
				msg:    tt.fields.msg,
			}
			if got := e.Marshal(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Error.Marshal() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_errType_String(t *testing.T) {
	tests := []struct {
		name string
		e    errType
		want string
	}{
		{
			name: "invalid argument >> That request is invalid",
			e:    TypeInvalidArgument,
			want: "That request is invalid",
		},
		{
			name: "not found >> The requested resource was not found",
			e:    TypeNotFound,
			want: "The requested resource was not found",
		},
		{
			name: "already exists >> That resource already exists",
			e:    TypeAlreadyExists,
			want: "That resource already exists",
		},
		{
			name: "permission denied >> You do not have permission to do that",
			e:    TypePermissionDenied,
			want: "You do not have permission to do that",
		},
		{
			name: "unauthenticated >> You must be authenticated to do that",
			e:    TypeUnauthenticated,
			want: "You must be authenticated to do that",
		},
		{
			name: "unimplemented >> That feature is not yet available",
			e:    TypeUnimplemented,
			want: "That feature is not yet available",
		},
		{
			name: "internal >> Something went wrong",
			e:    TypeInternal,
			want: "Something went wrong",
		},
		{
			name: "undefined >> Something went wrong",
			e:    TypeUndefined,
			want: "Something went wrong",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.e.String(); got != tt.want {
				t.Errorf("errType.String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_errType_HTTPStatusCode(t *testing.T) {
	tests := []struct {
		name string
		e    errType
		want int
	}{
		{
			name: "invalid argument >> 400",
			e:    TypeInvalidArgument,
			want: http.StatusBadRequest,
		},
		{
			name: "not found >> 404",
			e:    TypeNotFound,
			want: http.StatusNotFound,
		},
		{
			name: "already exists >> 409",
			e:    TypeAlreadyExists,
			want: http.StatusConflict,
		},
		{
			name: "permission denied >> 403",
			e:    TypePermissionDenied,
			want: http.StatusForbidden,
		},
		{
			name: "unauthenticated >> 401",
			e:    TypeUnauthenticated,
			want: http.StatusUnauthorized,
		},
		{
			name: "unimplemented >> 501",
			e:    TypeUnimplemented,
			want: http.StatusNotImplemented,
		},
		{
			name: "internal >> 500",
			e:    TypeInternal,
			want: http.StatusInternalServerError,
		},
		{
			name: "undefined >> 500",
			e:    TypeUndefined,
			want: http.StatusInternalServerError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.e.HTTPStatusCode(); got != tt.want {
				t.Errorf("errType.HTTPStatusCode() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNew(t *testing.T) {
	type args struct {
		typ     errType
		options []errOpt
	}
	tests := []struct {
		name string
		args args
		want Error
	}{
		{
			name: "default error",
			args: args{
				typ: TypeInternal,
			},
			want: Error{
				typ:    TypeInternal,
				pretty: false,
				genMsg: "Something went wrong",
				msg:    "",
			},
		},
		{
			name: "unimplemented",
			args: args{
				typ: TypeUnimplemented,
			},
			want: Error{
				typ:    TypeUnimplemented,
				pretty: false,
				genMsg: "That feature is not yet available",
				msg:    "",
			},
		},
		{
			name: "already exists with message",
			args: args{
				typ: TypeAlreadyExists,
				options: []errOpt{
					WithMsg("User with username 'david' already exists"),
				},
			},
			want: Error{
				typ:    TypeAlreadyExists,
				pretty: false,
				genMsg: "That resource already exists",
				msg:    "User with username 'david' already exists",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := New(tt.args.typ, tt.args.options...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("New() = %v, want %v", got, tt.want)
			}
		})
	}
}

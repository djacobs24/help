// Package errs provides the errors that should
// be returned from the application.
package errs

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// NewInvalidArgument constructs a new invalid argument error.
func NewInvalidArgument(options ...errOpt) Error {
	return New(TypeInvalidArgument, options...)
}

// NewNotFound constructs a new not found error.
func NewNotFound(options ...errOpt) Error {
	return New(TypeNotFound, options...)
}

// NewAlreadyExists constructs a new already exists error.
func NewAlreadyExists(options ...errOpt) Error {
	return New(TypeAlreadyExists, options...)
}

// NewPermissionDenied constructs a new permission denied error.
func NewPermissionDenied(options ...errOpt) Error {
	return New(TypePermissionDenied, options...)
}

// NewUnauthenticated constructs a new unauthenticated error.
func NewUnauthenticated(options ...errOpt) Error {
	return New(TypeUnauthenticated, options...)
}

// NewUnimplemented constructs a new unimplemented error.
func NewUnimplemented(options ...errOpt) Error {
	return New(TypeUnimplemented, options...)
}

// NewInternal constructs a new internal error.
func NewInternal(options ...errOpt) Error {
	return New(TypeInternal, options...)
}

// New constructs a new error.
func New(typ errType, options ...errOpt) Error {
	e := &Error{
		typ:    typ,
		genMsg: typ.String(),
	}

	// Apply any options
	for _, opt := range options {
		opt(e)
	}

	return *e
}

// errOpt is an optional parameter used when constructing Error's.
type errOpt func(e *Error)

// WithMsg appends a message that should NOT be presented to the user
func WithMsg(msg string) errOpt {
	return func(e *Error) {
		e.msg = msg
		e.pretty = false
	}
}

// WithMsgf appends a message that should NOT be presented to the user
func WithMsgf(format string, a ...interface{}) errOpt {
	return func(e *Error) {
		e.msg = fmt.Sprintf(format, a...)
		e.pretty = false
	}
}

// WithPrettyMsg appends a message that can be presented to the user
func WithPrettyMsg(msg string) errOpt {
	return func(e *Error) {
		e.msg = msg
		e.pretty = true
	}
}

// WithPrettyMsgf appends a message that can be presented to the user
func WithPrettyMsgf(format string, a ...interface{}) errOpt {
	return func(e *Error) {
		e.msg = fmt.Sprintf(format, a...)
		e.pretty = true
	}
}

// WithErr appends an error's message that should NOT be presented to the user
func WithErr(err error) errOpt {
	return func(e *Error) {
		e.msg = err.Error()
		e.pretty = false
	}
}

// WithPrettyErr appends an error's message that can be presented to the user
func WithPrettyErr(err error) errOpt {
	return func(e *Error) {
		e.msg = err.Error()
		e.pretty = true
	}
}

// Error is a standardized error for the application.
type Error struct {
	typ    errType
	pretty bool
	genMsg string
	msg    string
}

// Type returns the error type.
func (e Error) Type() errType {
	return e.typ
}

// IsPretty returns whether the error should be presented to the user.
func (e Error) IsPretty() bool {
	return e.pretty
}

// GenericMessage returns a generic message for the error type.
func (e Error) GenericMessage() string {
	return e.genMsg
}

// Error returns the error message.
func (e Error) Error() string {
	return e.msg
}

// Marshal returns the JSON representation of the error.
func (e Error) Marshal() []byte {
	b, _ := json.Marshal(struct {
		Pretty         bool   `json:"pretty"`
		GenericMessage string `json:"generic_message"`
		Message        string `json:"message"`
	}{
		Pretty:         e.IsPretty(),
		GenericMessage: e.GenericMessage(),
		Message:        e.Error(),
	})

	return b
}

// errType defines all error types returned from the application.
type errType string

const (
	TypeInvalidArgument  errType = "invalid_argument"
	TypeNotFound         errType = "not_found"
	TypeAlreadyExists    errType = "already_exists"
	TypePermissionDenied errType = "permission_denied"
	TypeUnauthenticated  errType = "unauthenticated"
	TypeUnimplemented    errType = "unimplemented"
	TypeInternal         errType = "internal"
	// Reserved for undefined errors
	TypeUndefined errType = "undefined"
)

// String returns a generic message for the error type.
func (e errType) String() string {
	switch e {
	case TypeInvalidArgument:
		return "That request is invalid"
	case TypeNotFound:
		return "The requested resource was not found"
	case TypeAlreadyExists:
		return "That resource already exists"
	case TypePermissionDenied:
		return "You do not have permission to do that"
	case TypeUnauthenticated:
		return "You must be authenticated to do that"
	case TypeUnimplemented:
		return "That feature is not yet available"
	default:
		return "Something went wrong"
	}
}

// HTTPStatusCode returns the HTTP status code for the error type.
func (e errType) HTTPStatusCode() int {
	switch e {
	case TypeInvalidArgument:
		return http.StatusBadRequest
	case TypeNotFound:
		return http.StatusNotFound
	case TypeAlreadyExists:
		return http.StatusConflict
	case TypePermissionDenied:
		return http.StatusForbidden
	case TypeUnauthenticated:
		return http.StatusUnauthorized
	case TypeUnimplemented:
		return http.StatusNotImplemented
	default:
		return http.StatusInternalServerError
	}
}

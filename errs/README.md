# errs

- Standardized errors
- Multi-message error generation

## Standardized Errors Usage
```golang
package main

import(
	"errors"
	"errs"
	"log"
)

// An specific error coming from the database
var ErrNotFound = errors.New("not found in db")

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}

func run() error {
	err := getUserFromDB()
	switch {
	case err == nil:
		return nil
	case errors.Is(err, ErrNotFound):
		return errs.NewNotFound(errs.WithPrettyMsg("User not found"))
	default:
		return errs.NewInternal(errs.WithMsgf("error getting user from db: %v", err))
	}
}

func getUserFromDB() error {
	return ErrNotFound
}
```

## Multi-Error Usage
```golang
package main

import (
	"errors"
	"errs"
	"log"
)

var ErrSomethingImportant = errors.New("something important failed")

func main() {
	err := doAllThings()

	multi := &errs.Multi{}
	switch {
	case err == nil:
		return
	case errors.As(err, &multi):
		if multi.Contains(ErrSomethingImportant) {
			log.Fatal(ErrSomethingImportant)
		}
		log.Fatal(multi.Error())
	default:
		log.Fatalf("error doing all the things: %s", err.Error())
	}
}

func doAllThings() error {
	m := errs.NewMulti(errors.New("error doing all the things"))

	if err := doThingOne(); err != nil {
		m.Append(err)
	}

	if err := doThingTwo(); err != nil {
		m.Append(err)
	}

	if err := doThingThree(); err != nil {
		m.Append(err)
	}

	if m.IsEmpty() {
		return nil
	}

	return m
}

func doThingOne() error {
	return errors.New("thing one failed to close a resource")
}

func doThingTwo() error {
	return nil
}

func doThingThree() error {
	return errors.New("thing two requires a field to be set")
}

```

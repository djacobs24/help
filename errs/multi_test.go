package errs

import (
	"errors"
	"fmt"
	"reflect"
	"testing"
)

var (
	err1 = errors.New("test error 1")
	err2 = errors.New("test error 2")
	err3 = errors.New("test error 3")
	err4 = errors.New("test error 4")
	err5 = errors.New("test error 5")
)

func TestNewMulti(t *testing.T) {
	type args struct {
		main error
	}
	tests := []struct {
		name string
		args args
		want *Multi
	}{
		{
			name: "with main error",
			args: args{
				main: err2,
			},
			want: &Multi{
				main: err2,
			},
		},
		{
			name: "without main error",
			args: args{
				main: nil,
			},
			want: &Multi{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewMulti(tt.args.main); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewMulti() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMulti_Error(t *testing.T) {
	type fields struct {
		main error
		errs []error
	}
	tests := []struct {
		name   string
		fields *fields
		want   string
	}{
		{
			name: "without main message",
			fields: &fields{
				main: nil,
				errs: []error{err1, err2},
			},
			want: fmt.Sprintf("%s; %s", err1.Error(), err2.Error()),
		},
		{
			name: "with main message",
			fields: &fields{
				main: err5,
				errs: []error{err1, err2},
			},
			want: fmt.Sprintf("%s: %s; %s", err5.Error(), err1.Error(), err2.Error()),
		},
		{
			name: "with main message only",
			fields: &fields{
				main: err5,
			},
			want: err5.Error(),
		},
		{
			name:   "without main and errs",
			fields: &fields{},
			want:   "",
		},
		{
			name:   "multi is nil",
			fields: nil,
			want:   "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var m *Multi
			if tt.fields != nil {
				m = &Multi{
					main: tt.fields.main,
					errs: tt.fields.errs,
				}
			}
			if got := m.Error(); got != tt.want {
				t.Errorf("Multi.Error() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMulti_Append(t *testing.T) {
	type fields struct {
		main error
		errs []error
	}
	type args struct {
		err error
	}
	tests := []struct {
		name   string
		fields *fields
		args   args
		want   *Multi
	}{
		{
			name: "appends error - with existing errs",
			fields: &fields{
				main: nil,
				errs: []error{err1, err2},
			},
			args: args{
				err: err3,
			},
			want: &Multi{
				errs: []error{err1, err2, err3},
			},
		},
		{
			name: "appends error - without existing errs",
			fields: &fields{
				main: nil,
				errs: []error{},
			},
			args: args{
				err: err4,
			},
			want: &Multi{
				errs: []error{err4},
			},
		},
		{
			name: "appends error - with nil errs",
			fields: &fields{
				main: nil,
				errs: nil,
			},
			args: args{
				err: err4,
			},
			want: &Multi{
				errs: []error{err4},
			},
		},
		{
			name:   "multi is nil",
			fields: nil,
			args: args{
				err: err4,
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var m *Multi
			if tt.fields != nil {
				m = &Multi{
					main: tt.fields.main,
					errs: tt.fields.errs,
				}
			}
			m.Append(tt.args.err)
			if !reflect.DeepEqual(m, tt.want) {
				t.Errorf("Multi.Append() = %v, want %v", m, tt.want)
			}
		})
	}
}

func TestMulti_Contains(t *testing.T) {
	type fields struct {
		main error
		errs []error
	}
	type args struct {
		err error
	}
	tests := []struct {
		name   string
		fields *fields
		args   args
		want   bool
	}{
		{
			name: "does contain error",
			fields: &fields{
				main: nil,
				errs: []error{err1, err2, err3},
			},
			args: args{
				err: err3,
			},
			want: true,
		},
		{
			name: "does not contain error",
			fields: &fields{
				main: nil,
				errs: []error{err1, err2, err3},
			},
			args: args{
				err: err5,
			},
			want: false,
		},
		{
			name: "does not contain error - nil",
			fields: &fields{
				main: nil,
				errs: nil,
			},
			args: args{
				err: err1,
			},
			want: false,
		},
		{
			name:   "multi is nil",
			fields: nil,
			args: args{
				err: err1,
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var m *Multi
			if tt.fields != nil {
				m = &Multi{
					main: tt.fields.main,
					errs: tt.fields.errs,
				}
			}
			if got := m.Contains(tt.args.err); got != tt.want {
				t.Errorf("Multi.Contains() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMulti_Errors(t *testing.T) {
	type fields struct {
		main error
		errs []error
	}
	tests := []struct {
		name   string
		fields *fields
		want   []error
	}{
		{
			name: "returns nil errors",
			fields: &fields{
				main: nil,
				errs: nil,
			},
			want: nil,
		},
		{
			name: "returns no errors",
			fields: &fields{
				main: nil,
				errs: []error{},
			},
			want: []error{},
		},
		{
			name: "returns all 3 errors",
			fields: &fields{
				main: nil,
				errs: []error{err1, err2, err3},
			},
			want: []error{err1, err2, err3},
		},
		{
			name:   "multi is nil",
			fields: nil,
			want:   nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var m *Multi
			if tt.fields != nil {
				m = &Multi{
					main: tt.fields.main,
					errs: tt.fields.errs,
				}
			}
			if got := m.Errors(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Multi.Errors() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMulti_IsEmpty(t *testing.T) {
	type fields struct {
		main error
		errs []error
	}
	tests := []struct {
		name   string
		fields *fields
		want   bool
	}{
		{
			name: "is empty - nil errs",
			fields: &fields{
				main: nil,
				errs: nil,
			},
			want: true,
		},
		{
			name: "is empty - empty errs",
			fields: &fields{
				main: nil,
				errs: []error{},
			},
			want: true,
		},
		{
			name: "is not empty",
			fields: &fields{
				main: nil,
				errs: []error{err1},
			},
			want: false,
		},
		{
			name:   "multi is nil",
			fields: nil,
			want:   true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var m *Multi
			if tt.fields != nil {
				m = &Multi{
					main: tt.fields.main,
					errs: tt.fields.errs,
				}
			}
			if got := m.IsEmpty(); got != tt.want {
				t.Errorf("Multi.IsEmpty() = %v, want %v", got, tt.want)
			}
		})
	}
}

package errs

import (
	"errors"
	"strings"
)

// Multi is an error that consists of multiple errors.
type Multi struct {
	main error
	errs []error
}

// NewMulti constructs a new Multi error.
func NewMulti(main error) *Multi {
	return &Multi{
		main: main,
	}
}

// Error returns Multi's error message.
// [main_message]
// [main_message]: [error_one]; [error_two]
func (m *Multi) Error() string {
	if m == nil {
		return ""
	}

	var msg string
	if m.main != nil {
		msg += strings.TrimSpace(m.main.Error())

		if len(m.errs) == 0 {
			// Only have main error.
			return msg
		}

		if !strings.HasSuffix(msg, ":") {
			msg += ":"
		}
	}

	errs := m.errs
	for i, err := range errs {
		if i != 0 {
			msg += ";"
		}

		if msg != "" {
			msg += " "
		}

		msg += err.Error()
	}

	return msg
}

// Append adds an error to Multi's errors.
func (m *Multi) Append(err error) {
	if m == nil {
		return
	}

	m.errs = append(m.errs, err)
}

// Contains returns true if the Multi error
// contains the given error.
func (m *Multi) Contains(err error) bool {
	if m == nil {
		return false
	}

	for _, e := range m.errs {
		if errors.Is(e, err) {
			return true
		}
	}

	return false
}

// Errors returns Multi's errors.
func (m *Multi) Errors() []error {
	if m == nil {
		return nil
	}

	return m.errs
}

// IsEmpty returns true if Multi's errors
// are empty.
func (m *Multi) IsEmpty() bool {
	if m == nil {
		return true
	}

	return len(m.errs) == 0
}

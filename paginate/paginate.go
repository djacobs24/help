package paginate

// Pagination defines how results are paginated.
type Pagination struct {
	Page    uint
	PerPage uint
	Total   uint64
}

// Next returns the next pagination item
// or the same item if this is the last page.
func (p *Pagination) Next() *Pagination {
	if p == nil {
		return nil
	}

	if !p.HasNext() {
		return p
	}

	return &Pagination{
		Page:    p.Page + 1,
		PerPage: p.PerPage,
		Total:   p.Total,
	}
}

// HasNext returns true if this is not the last page.
func (p *Pagination) HasNext() bool {
	if p == nil {
		return false
	}

	return uint64(p.Page*p.PerPage) < p.Total
}

// Previous returns the previous pagination item
// or the same item if this is the first page.
func (p *Pagination) Previous() *Pagination {
	if p == nil {
		return nil
	}

	if !p.HasPrevious() {
		return p
	}

	return &Pagination{
		Page:    p.Page - 1,
		PerPage: p.PerPage,
		Total:   p.Total,
	}
}

// HasPrevious returns true if this is not the first page.
func (p *Pagination) HasPrevious() bool {
	if p == nil {
		return false
	}

	return p.Page > 1
}

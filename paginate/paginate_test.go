package paginate

import (
	"reflect"
	"testing"
)

func TestPagination_HasNext(t *testing.T) {
	tests := []struct {
		name       string
		pagination *Pagination
		want       bool
	}{
		{
			name: "has next",
			pagination: &Pagination{
				Page:    1,
				PerPage: 5,
				Total:   6,
			},
			want: true,
		},
		{
			name: "does not have next",
			pagination: &Pagination{
				Page:    2,
				PerPage: 5,
				Total:   6,
			},
			want: false,
		},
		{
			name:       "nil returns false",
			pagination: nil,
			want:       false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.pagination.HasNext(); got != tt.want {
				t.Errorf("Pagination.HasNext() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPagination_Next(t *testing.T) {
	tests := []struct {
		name       string
		pagination *Pagination
		want       *Pagination
	}{
		{
			name: "returns next when there is next",
			pagination: &Pagination{
				Page:    1,
				PerPage: 7,
				Total:   16,
			},
			want: &Pagination{
				Page:    2,
				PerPage: 7,
				Total:   16,
			},
		},
		{
			name: "returns same when there is not next",
			pagination: &Pagination{
				Page:    3,
				PerPage: 7,
				Total:   16,
			},
			want: &Pagination{
				Page:    3,
				PerPage: 7,
				Total:   16,
			},
		},
		{
			name:       "nil returns nil",
			pagination: nil,
			want:       nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.pagination.Next(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Pagination.Next() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPagination_HasPrevious(t *testing.T) {
	tests := []struct {
		name       string
		pagination *Pagination
		want       bool
	}{
		{
			name: "first page does not have previous",
			pagination: &Pagination{
				Page:    1,
				PerPage: 5,
				Total:   100,
			},
			want: false,
		},
		{
			name: "second page does have previous",
			pagination: &Pagination{
				Page:    2,
				PerPage: 5,
				Total:   100,
			},
			want: true,
		},
		{
			name:       "nil returns false",
			pagination: nil,
			want:       false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.pagination.HasPrevious(); got != tt.want {
				t.Errorf("Pagination.HasPrevious() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPagination_Previous(t *testing.T) {
	tests := []struct {
		name       string
		pagination *Pagination
		want       *Pagination
	}{
		{
			name: "returns previous when there is previous",
			pagination: &Pagination{
				Page:    2,
				PerPage: 7,
				Total:   16,
			},
			want: &Pagination{
				Page:    1,
				PerPage: 7,
				Total:   16,
			},
		},
		{
			name: "returns same when there is not previous",
			pagination: &Pagination{
				Page:    1,
				PerPage: 7,
				Total:   16,
			},
			want: &Pagination{
				Page:    1,
				PerPage: 7,
				Total:   16,
			},
		},
		{
			name:       "nil returns nil",
			pagination: nil,
			want:       nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.pagination.Previous(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Pagination.Previous() = %v, want %v", got, tt.want)
			}
		})
	}
}

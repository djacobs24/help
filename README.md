# Help

Help consists of helpers that I use often in my Go development.

## Helpers Documentation

[`env`](./env/README.md) - Environment variable lookup

[`errs`](./errs/README.md) - Standardized errors and multi-message error generation

[`paginate`](./paginate/README.md) - Standardized pagination

[`rand`](./rand/README.md) - Random string generation

[`validate`](./validate/README.md) - Validation
package validate

import (
	"errors"
	"strings"
)

// replaceNth replaces the nth occurrence of a string.
func replaceNth(str, old, new string, n int) string {
	i := 0
	for m := 1; m <= n; m++ {
		x := strings.Index(str[i:], old)
		if x < 0 {
			break
		}
		i += x
		if m == n {
			return str[:i] + new + str[i+len(old):]
		}
		i += len(old)
	}

	return str
}

// getFieldNamesWithNumValues parses a slice of strings and returns the
// field names joined and number of non-empty values.
func getFieldNamesWithNumValues(fieldNamesAndValues []string) (string, int, error) {
	var fieldNames []string
	var numValues int

	numFields := len(fieldNamesAndValues)
	if numFields%2 != 0 {
		return "", numValues, errors.New("invalid number of arguments provided")
	}

	for i := 0; i < numFields; i++ {
		isFieldName := i%2 == 0
		if isFieldName {
			fieldNames = append(fieldNames, fieldNamesAndValues[i])
			continue
		}

		if fieldNamesAndValues[i] != "" {
			numValues++
		}
	}

	joinedFields := strings.Join(fieldNames, ", ")
	numCommas := strings.Count(joinedFields, ", ")
	prettyFieldNames := replaceNth(joinedFields, ", ", " or ", numCommas)

	return prettyFieldNames, numValues, nil
}

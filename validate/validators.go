package validate

import (
	"errors"
	"fmt"
	"strings"

	"github.com/google/uuid"
)

// Validator provides a functional and easily
// extendable way of defining validators.
type Validator func() error

//========================================
// String
//========================================

// StringProvided validates that a string value was provided.
func StringProvided(fieldName, fieldValue string) Validator {
	return func() error {
		if fieldValue == "" {
			return fmt.Errorf("%s is a required field", fieldName)
		}

		return nil
	}
}

// StringLenGreaterThan validates that a string length is greater
// than the value provided.
func StringLenGreaterThan(fieldName, fieldValue string, greaterThan int) Validator {
	return func() error {
		if len(fieldValue) <= greaterThan {
			return fmt.Errorf("%s must exceed %d characters", fieldName, greaterThan)
		}

		return nil
	}
}

// StringLenLessThan validates that a string is less than the value provided.
func StringLenLessThan(fieldName, fieldValue string, lessThan int) Validator {
	return func() error {
		if len(fieldValue) >= lessThan {
			return fmt.Errorf("%s must not exceed %d characters", fieldName, lessThan-1)
		}

		return nil
	}
}

// StringLenBetween validates that a string length is between the provided values.
func StringLenBetween(fieldName, fieldValue string, min, max int) Validator {
	return func() error {
		if min > max {
			return fmt.Errorf("min cannot be greater than max")
		}

		if len(fieldValue) < min || len(fieldValue) > max {
			return fmt.Errorf("%s must be between %d and %d characters", fieldName, min, max)
		}

		return nil
	}
}

// StringsAreEqual validates that strings are equal.
func StringsAreEqual(fieldName1, fieldValue1, fieldName2, fieldValue2 string) Validator {
	return func() error {
		if fieldValue1 != fieldValue2 {
			return fmt.Errorf("%s and %s must match", fieldName1, fieldName2)
		}

		return nil
	}
}

// StringsAreNotEqual validates that strings are not equal.
func StringsAreNotEqual(fieldName1, fieldValue1, fieldName2, fieldValue2 string) Validator {
	return func() error {
		if fieldValue1 == fieldValue2 {
			return fmt.Errorf("%s and %s cannot match", fieldName1, fieldName2)
		}

		return nil
	}
}

// StringContains validates that a string contains a substring.
func StringContains(fieldName, fieldValue, contains string) Validator {
	return func() error {
		if !strings.Contains(fieldValue, contains) {
			return fmt.Errorf("%s must contain %q", fieldName, contains)
		}

		return nil
	}
}

// OneStringExists validates at least one field is populated.
// Arguments must be provided fieldName, fieldValue, ...
func OneStringExists(fieldNamesAndValues ...string) Validator {
	return func() error {
		if len(fieldNamesAndValues) == 0 {
			return errors.New("no fields provided")
		}

		fieldNames, numValues, err := getFieldNamesWithNumValues(fieldNamesAndValues)
		if err != nil {
			return err
		}

		if numValues <= 0 {
			return fmt.Errorf("%s is required", fieldNames)
		}

		return nil
	}
}

// MultipleStringsDoNotExist validates only zero or one fields are populated.
// Arguments must be provided fieldName, fieldValue, ...
func MultipleStringsDoNotExist(fieldNamesAndValues ...string) Validator {
	return func() error {
		if len(fieldNamesAndValues) == 0 || len(fieldNamesAndValues) == 1 {
			return nil
		}

		fieldNames, numValues, err := getFieldNamesWithNumValues(fieldNamesAndValues)
		if err != nil {
			return err
		}

		if numValues > 1 {
			return fmt.Errorf("only one of %s can be provided", fieldNames)
		}

		return nil
	}
}

//========================================
// Integer
//========================================

// IntGreaterThan validates that an int is greater than the provided value.
func IntGreaterThan(fieldName string, fieldValue, greaterThan int) Validator {
	return func() error {
		if fieldValue <= greaterThan {
			return fmt.Errorf("%s must be at least %d", fieldName, greaterThan+1)
		}

		return nil
	}
}

// IntLessThan validates that an int is less than the provided value.
func IntLessThan(fieldName string, fieldValue, lessThan int) Validator {
	return func() error {
		if fieldValue >= lessThan {
			return fmt.Errorf("%s cannot exceed %d", fieldName, lessThan-1)
		}

		return nil
	}
}

// IntBetween validates that an int is between the provided values.
func IntBetween(fieldName string, fieldValue, min, max int) Validator {
	return func() error {
		if min > max {
			return fmt.Errorf("min cannot be greater than max")
		}

		if fieldValue < min || fieldValue > max {
			return fmt.Errorf("%s must be between %d and %d", fieldName, min, max)
		}

		return nil
	}
}

//========================================
// Boolean
//========================================

// BoolProvided validates that a boolean value was provided.
func BoolProvided(fieldName string, fieldValue *bool) Validator {
	return func() error {
		if fieldValue == nil {
			return fmt.Errorf("%s is a required field", fieldName)
		}

		return nil
	}
}

//========================================
// UUID
//========================================

func UUID(fieldName, fieldValue string, id *uuid.UUID) Validator {
	return func() error {
		if err := StringProvided(fieldName, fieldValue)(); err != nil {
			return err
		}

		u, err := uuid.Parse(fieldValue)
		if err != nil {
			return fmt.Errorf("%q is not a valid uuid", fieldValue)
		}

		id = &u

		return nil
	}
}

package validate_test

import (
	"errors"
	"fmt"
	"testing"

	"gitlab.com/djacobs24/help/validate"
)

func compareValidatorError(validator validate.Validator, want error) error {
	got := validator()

	if got == nil {
		if want == nil {
			return nil
		}

		return fmt.Errorf("got: nil, wanted: %q", want.Error())
	}

	if want == nil {
		return fmt.Errorf("got: %q, wanted: nil", got.Error())
	}

	if got.Error() != want.Error() {
		return fmt.Errorf("got: %q, wanted: %q", got.Error(), want.Error())
	}

	return nil
}

func TestStringProvided(t *testing.T) {
	type args struct {
		fieldName  string
		fieldValue string
	}
	tests := []struct {
		name string
		args args
		want error
	}{
		{
			name: "error if no string provided",
			args: args{
				fieldName:  "Name",
				fieldValue: "",
			},
			want: errors.New("Name is a required field"),
		},
		{
			name: "no error if string provided",
			args: args{
				fieldName:  "Name",
				fieldValue: "David",
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			validator := validate.StringProvided(
				tt.args.fieldName,
				tt.args.fieldValue,
			)
			if err := compareValidatorError(validator, tt.want); err != nil {
				t.Error(err)
			}
		})
	}
}

func TestStringLenGreaterThan(t *testing.T) {
	type args struct {
		fieldName   string
		fieldValue  string
		greaterThan int
	}
	tests := []struct {
		name string
		args args
		want error
	}{
		{
			name: "empty value fails",
			args: args{
				fieldName:   "Password",
				fieldValue:  "",
				greaterThan: 10,
			},
			want: errors.New("Password must exceed 10 characters"),
		},
		{
			name: "less than length fails",
			args: args{
				fieldName:   "Password",
				fieldValue:  "password12",
				greaterThan: 10,
			},
			want: errors.New("Password must exceed 10 characters"),
		},
		{
			name: "exact length passes",
			args: args{
				fieldName:   "Password",
				fieldValue:  "password123",
				greaterThan: 10,
			},
			want: nil,
		},
		{
			name: "greater than length passes",
			args: args{
				fieldName:   "Password",
				fieldValue:  "password1234",
				greaterThan: 10,
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			validator := validate.StringLenGreaterThan(
				tt.args.fieldName,
				tt.args.fieldValue,
				tt.args.greaterThan,
			)
			if err := compareValidatorError(validator, tt.want); err != nil {
				t.Error(err)
			}
		})
	}
}

func TestStringLenLessThan(t *testing.T) {
	type args struct {
		fieldName  string
		fieldValue string
		lessThan   int
	}
	tests := []struct {
		name string
		args args
		want error
	}{
		{
			name: "empty value fails",
			args: args{
				fieldName:  "Password",
				fieldValue: "",
				lessThan:   0,
			},
			want: errors.New("Password must not exceed -1 characters"),
		},
		{
			name: "greater than length fails",
			args: args{
				fieldName:  "Password",
				fieldValue: "password12345678",
				lessThan:   16,
			},
			want: errors.New("Password must not exceed 15 characters"),
		},
		{
			name: "exact length passes",
			args: args{
				fieldName:  "Password",
				fieldValue: "password1234567",
				lessThan:   16,
			},
			want: nil,
		},
		{
			name: "less than length passes",
			args: args{
				fieldName:  "Password",
				fieldValue: "password12345",
				lessThan:   16,
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			validator := validate.StringLenLessThan(
				tt.args.fieldName,
				tt.args.fieldValue,
				tt.args.lessThan,
			)
			if err := compareValidatorError(validator, tt.want); err != nil {
				t.Error(err)
			}
		})
	}
}

func TestStringLenBetween(t *testing.T) {
	type args struct {
		fieldName  string
		fieldValue string
		min        int
		max        int
	}
	tests := []struct {
		name string
		args args
		want error
	}{
		{
			name: "valid length",
			args: args{
				fieldName:  "Name",
				fieldValue: "David",
				min:        1,
				max:        8,
			},
			want: nil,
		},
		{
			name: "min greater than max",
			args: args{
				fieldName:  "Name",
				fieldValue: "David",
				min:        9,
				max:        8,
			},
			want: errors.New("min cannot be greater than max"),
		},
		{
			name: "greater than",
			args: args{
				fieldName:  "Name",
				fieldValue: "DavidDavi",
				min:        1,
				max:        8,
			},
			want: errors.New("Name must be between 1 and 8 characters"),
		},
		{
			name: "less than",
			args: args{
				fieldName:  "Name",
				fieldValue: "D",
				min:        2,
				max:        8,
			},
			want: errors.New("Name must be between 2 and 8 characters"),
		},
		{
			name: "valid length - min",
			args: args{
				fieldName:  "Name",
				fieldValue: "Da",
				min:        2,
				max:        8,
			},
			want: nil,
		},
		{
			name: "valid length - max",
			args: args{
				fieldName:  "Name",
				fieldValue: "DavidDav",
				min:        2,
				max:        8,
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			validator := validate.StringLenBetween(
				tt.args.fieldName,
				tt.args.fieldValue,
				tt.args.min,
				tt.args.max,
			)
			if err := compareValidatorError(validator, tt.want); err != nil {
				t.Error(err)
			}
		})
	}
}

func TestStringsAreEqual(t *testing.T) {
	type args struct {
		fieldName1  string
		fieldValue1 string
		fieldName2  string
		fieldValue2 string
	}
	tests := []struct {
		name string
		args args
		want error
	}{
		{
			name: "strings match",
			args: args{
				fieldName1:  "Password",
				fieldValue1: "pass123",
				fieldName2:  "Password Confirmation",
				fieldValue2: "pass123",
			},
			want: nil,
		},
		{
			name: "strings match - empty",
			args: args{
				fieldName1:  "Password",
				fieldValue1: "",
				fieldName2:  "Password Confirmation",
				fieldValue2: "",
			},
			want: nil,
		},
		{
			name: "strings do not match",
			args: args{
				fieldName1:  "Password",
				fieldValue1: "asdf",
				fieldName2:  "Password Confirmation",
				fieldValue2: "asdf4",
			},
			want: errors.New("Password and Password Confirmation must match"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			validator := validate.StringsAreEqual(
				tt.args.fieldName1,
				tt.args.fieldValue1,
				tt.args.fieldName2,
				tt.args.fieldValue2,
			)
			if err := compareValidatorError(validator, tt.want); err != nil {
				t.Error(err)
			}
		})
	}
}

func TestStringsAreNotEqual(t *testing.T) {
	type args struct {
		fieldName1  string
		fieldValue1 string
		fieldName2  string
		fieldValue2 string
	}
	tests := []struct {
		name string
		args args
		want error
	}{
		{
			name: "strings match",
			args: args{
				fieldName1:  "Password",
				fieldValue1: "pass123",
				fieldName2:  "Password Confirmation",
				fieldValue2: "pass123",
			},
			want: errors.New("Password and Password Confirmation cannot match"),
		},
		{
			name: "strings match - empty",
			args: args{
				fieldName1:  "Password",
				fieldValue1: "",
				fieldName2:  "Password Confirmation",
				fieldValue2: "",
			},
			want: errors.New("Password and Password Confirmation cannot match"),
		},
		{
			name: "strings do not match",
			args: args{
				fieldName1:  "Password",
				fieldValue1: "asdf",
				fieldName2:  "Password Confirmation",
				fieldValue2: "asdf4",
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			validator := validate.StringsAreNotEqual(
				tt.args.fieldName1,
				tt.args.fieldValue1,
				tt.args.fieldName2,
				tt.args.fieldValue2,
			)
			if err := compareValidatorError(validator, tt.want); err != nil {
				t.Error(err)
			}
		})
	}
}

func TestStringContains(t *testing.T) {
	type args struct {
		fieldName  string
		fieldValue string
		contains   string
	}
	tests := []struct {
		name string
		args args
		want error
	}{
		{
			name: "value contains character",
			args: args{
				fieldName:  "Name",
				fieldValue: "David",
				contains:   "d",
			},
			want: nil,
		},
		{
			name: "value does not character",
			args: args{
				fieldName:  "Name",
				fieldValue: "David",
				contains:   "c",
			},
			want: errors.New("Name must contain \"c\""),
		},
		{
			name: "empty value does not character",
			args: args{
				fieldName:  "Name",
				fieldValue: "",
				contains:   "h",
			},
			want: errors.New("Name must contain \"h\""),
		},
		{
			name: "empty 'contains' returns nil",
			args: args{
				fieldName:  "Name",
				fieldValue: "David",
				contains:   "",
			},
			want: nil,
		},
		{
			name: "empty 'value' and 'contains' returns nil",
			args: args{
				fieldName:  "Name",
				fieldValue: "",
				contains:   "",
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			validator := validate.StringContains(
				tt.args.fieldName,
				tt.args.fieldValue,
				tt.args.contains,
			)
			if err := compareValidatorError(validator, tt.want); err != nil {
				t.Error(err)
			}
		})
	}
}

func TestOneStringExists(t *testing.T) {
	type args struct {
		fieldNamesAndValues []string
	}
	tests := []struct {
		name string
		args args
		want error
	}{
		{
			name: "multiple fields and values exist returns nil",
			args: args{
				fieldNamesAndValues: []string{
					"Field One", "Value One",
					"Field Two", "Value Two",
					"Field Three", "Value Three",
				},
			},
			want: nil,
		},
		{
			name: "no fields or values exist returns error",
			args: args{
				fieldNamesAndValues: []string{},
			},
			want: errors.New("no fields provided"),
		},
		{
			name: "only one field provided",
			args: args{
				fieldNamesAndValues: []string{
					"Field One",
				},
			},
			want: errors.New("invalid number of arguments provided"),
		},
		{
			name: "multiple fields with empty values returns error",
			args: args{
				fieldNamesAndValues: []string{
					"Field One", "",
					"Field Two", "",
					"Field Three", "",
				},
			},
			want: errors.New("Field One, Field Two or Field Three is required"),
		},
		{
			name: "multiple fields with one value returns nil",
			args: args{
				fieldNamesAndValues: []string{
					"Field One", "",
					"Field Two", "Value Two",
					"Field Three", "",
				},
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			validator := validate.OneStringExists(
				tt.args.fieldNamesAndValues...,
			)
			if err := compareValidatorError(validator, tt.want); err != nil {
				t.Error(err)
			}
		})
	}
}

func TestMultipleStringsDoNotExist(t *testing.T) {
	type args struct {
		fieldNamesAndValues []string
	}
	tests := []struct {
		name string
		args args
		want error
	}{
		{
			name: "multiple fields and values exist returns error",
			args: args{
				fieldNamesAndValues: []string{
					"Field One", "Value One",
					"Field Two", "Value Two",
					"Field Three", "Value Three",
				},
			},
			want: errors.New("only one of Field One, Field Two or Field Three can be provided"),
		},
		{
			name: "no fields or values exist returns nil",
			args: args{
				fieldNamesAndValues: []string{},
			},
			want: nil,
		},
		{
			name: "only one field provided returns nil",
			args: args{
				fieldNamesAndValues: []string{
					"Field One",
				},
			},
			want: nil,
		},
		{
			name: "multiple fields with empty values returns nil",
			args: args{
				fieldNamesAndValues: []string{
					"Field One", "",
					"Field Two", "",
					"Field Three", "",
				},
			},
			want: nil,
		},
		{
			name: "multiple fields with one value returns nil",
			args: args{
				fieldNamesAndValues: []string{
					"Field One", "",
					"Field Two", "Value Two",
					"Field Three", "",
				},
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			validator := validate.MultipleStringsDoNotExist(
				tt.args.fieldNamesAndValues...,
			)
			if err := compareValidatorError(validator, tt.want); err != nil {
				t.Error(err)
			}
		})
	}
}

func TestIntGreaterThan(t *testing.T) {
	type args struct {
		fieldName   string
		fieldValue  int
		greaterThan int
	}
	tests := []struct {
		name string
		args args
		want error
	}{
		{
			name: "value same as greaterThan returns error",
			args: args{
				fieldName:   "Age",
				fieldValue:  17,
				greaterThan: 17,
			},
			want: errors.New("Age must be at least 18"),
		},
		{
			name: "value less than greaterThan returns error",
			args: args{
				fieldName:   "Age",
				fieldValue:  16,
				greaterThan: 17,
			},
			want: errors.New("Age must be at least 18"),
		},
		{
			name: "value greater than greaterThan returns nil",
			args: args{
				fieldName:   "Age",
				fieldValue:  18,
				greaterThan: 17,
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			validator := validate.IntGreaterThan(
				tt.args.fieldName,
				tt.args.fieldValue,
				tt.args.greaterThan,
			)
			if err := compareValidatorError(validator, tt.want); err != nil {
				t.Error(err)
			}
		})
	}
}

func TestIntLessThan(t *testing.T) {
	type args struct {
		fieldName  string
		fieldValue int
		lessThan   int
	}
	tests := []struct {
		name string
		args args
		want error
	}{
		{
			name: "value same as lessThan returns error",
			args: args{
				fieldName:  "Age",
				fieldValue: 50,
				lessThan:   50,
			},
			want: errors.New("Age cannot exceed 49"),
		},
		{
			name: "value less than lessThan returns nil",
			args: args{
				fieldName:  "Age",
				fieldValue: 49,
				lessThan:   50,
			},
			want: nil,
		},
		{
			name: "value greater than lessThan returns error",
			args: args{
				fieldName:  "Age",
				fieldValue: 51,
				lessThan:   50,
			},
			want: errors.New("Age cannot exceed 49"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			validator := validate.IntLessThan(
				tt.args.fieldName,
				tt.args.fieldValue,
				tt.args.lessThan,
			)
			if err := compareValidatorError(validator, tt.want); err != nil {
				t.Error(err)
			}
		})
	}
}

func TestIntBetween(t *testing.T) {
	type args struct {
		fieldName  string
		fieldValue int
		min        int
		max        int
	}
	tests := []struct {
		name string
		args args
		want error
	}{
		{
			name: "valid length",
			args: args{
				fieldName:  "Age",
				fieldValue: 28,
				min:        27,
				max:        29,
			},
			want: nil,
		},
		{
			name: "min greater than max",
			args: args{
				fieldName:  "Age",
				fieldValue: 28,
				min:        9,
				max:        8,
			},
			want: errors.New("min cannot be greater than max"),
		},
		{
			name: "greater than",
			args: args{
				fieldName:  "Age",
				fieldValue: 28,
				min:        1,
				max:        27,
			},
			want: errors.New("Age must be between 1 and 27"),
		},
		{
			name: "less than",
			args: args{
				fieldName:  "Age",
				fieldValue: 28,
				min:        29,
				max:        88,
			},
			want: errors.New("Age must be between 29 and 88"),
		},
		{
			name: "valid length - min",
			args: args{
				fieldName:  "Age",
				fieldValue: 28,
				min:        28,
				max:        30,
			},
			want: nil,
		},
		{
			name: "valid length - max",
			args: args{
				fieldName:  "Age",
				fieldValue: 28,
				min:        2,
				max:        28,
			},
			want: nil,
		}}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			validator := validate.IntBetween(
				tt.args.fieldName,
				tt.args.fieldValue,
				tt.args.min,
				tt.args.max,
			)
			if err := compareValidatorError(validator, tt.want); err != nil {
				t.Error(err)
			}
		})
	}
}

func TestBoolProvided(t *testing.T) {
	var provided = true
	var notProvided *bool

	type args struct {
		fieldName  string
		fieldValue *bool
	}
	tests := []struct {
		name string
		args args
		want error
	}{
		{
			name: "bool provided returns nil",
			args: args{
				fieldName:  "IsHavingFun",
				fieldValue: &provided,
			},
			want: nil,
		},
		{
			name: "bool not provided returns error",
			args: args{
				fieldName:  "IsHavingFun",
				fieldValue: notProvided,
			},
			want: errors.New("IsHavingFun is a required field"),
		},
		{
			name: "bool not provided returns error",
			args: args{
				fieldName: "IsHavingFun",
			},
			want: errors.New("IsHavingFun is a required field"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			validator := validate.BoolProvided(
				tt.args.fieldName,
				tt.args.fieldValue,
			)
			if err := compareValidatorError(validator, tt.want); err != nil {
				t.Error(err)
			}
		})
	}
}

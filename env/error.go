package env

import (
	"errors"
	"fmt"
)

var (
	// ErrInvalidType is returned when the value found is
	// not a valid type.
	ErrInvalidType = errors.New("invalid type")
)

// ErrKeyNotSet is returned when the key is not found.
type ErrKeyNotSet struct {
	key string
}

func (e ErrKeyNotSet) Error() string {
	return fmt.Sprintf("%q not set", e.key)
}

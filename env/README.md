# env

Environment variable lookup

## Usage

```golang
package main

import (
	"gitlab.com/djacobs24/help/env"
)


func main() {
    port := env.LookupIntWithDefault("PORT", 8080)
    addr, err := env.LookupString("ADDR")
    if err != nil {
        log.Fatal(err)
    }
}

```
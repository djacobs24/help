package env

import (
	"os"
	"strconv"
	"strings"
)

// LookupString looks up the key provided.
// Errors: ErrKeyNotSet
func LookupString(key string) (string, error) {
	return lookup(key)
}

// LookupIntWithDefault looks up the key provided, if the value
// does not exist, it returns the default value provided.
func LookupStringWithDefault(key, def string) string {
	val, err := lookup(key)
	if err != nil {
		return def
	}

	return val
}

// LookupInt looks up the key provided and attempts to cast the
// value found to an integer type.
// Errors: ErrKeyNotSet, ErrInvalidType
func LookupInt(key string) (int, error) {
	return lookupInt(key)
}

// LookupIntWithDefault looks up the key provided, if the value
// does not exist or is the wrong type, it returns the default
// value provided.
func LookupIntWithDefault(key string, def int) int {
	val, err := lookupInt(key)
	if err != nil {
		return def
	}

	return val
}

// LookupBool looks up the key provided and returns true if
// the value is 'true' or '1'.
// Errors: ErrKeyNotSet, ErrInvalidType
func LookupBool(key string) (bool, error) {
	return lookupBool(key)
}

// LookupBoolWithDefault looks up the key provided, if the value
// does not exist or is the wrong type, it returns the default
// value provided.
func LookupBoolWithDefault(key string, def bool) bool {
	val, err := lookupBool(key)
	if err != nil {
		return def
	}

	return val
}

func lookup(key string) (string, error) {
	val, ok := os.LookupEnv(key)
	if !ok {
		return "", ErrKeyNotSet{key: key}
	}

	return val, nil
}

func lookupInt(key string) (int, error) {
	val, err := lookup(key)
	if err != nil {
		return 0, err
	}

	intVal, err := strconv.Atoi(val)
	if err != nil {
		return 0, ErrInvalidType
	}

	return intVal, nil
}

func lookupBool(key string) (bool, error) {
	val, err := lookup(key)
	if err != nil {
		return false, err
	}

	val = strings.ToLower(val)
	if val != "true" && val != "false" && val != "1" && val != "0" {
		return false, ErrInvalidType
	}

	return strings.ToLower(val) == "true" || val == "1", nil
}

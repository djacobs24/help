package env_test

import (
	"errors"
	"os"
	"testing"

	"gitlab.com/djacobs24/help/env"
)

const (
	stringKey = "A_VERY_GOOD_STRING_ENV_KEY"
	intKey    = "AN_EVEN_BETTER_INT_ENV_VAR"
	boolKey   = "THE_BEST_BOOL_ENV_VAR"
)

func TestLookupStringErrKeyNotSet(t *testing.T) {
	_, err := env.LookupString(stringKey)
	if !errors.As(err, &env.ErrKeyNotSet{}) {
		t.Errorf("env.LookupString(): got err %v, expected err %s", err, "ErrKeyNotSet")
	}
}

func TestLookupStringWithDefaultUsesDefault(t *testing.T) {
	val := env.LookupStringWithDefault(stringKey, "hello")
	if val != "hello" {
		t.Errorf("env.LookupString(): got %q, expected %q", val, "hello")
	}
}

func TestLookupStringWithDefaultDoesNotUseDefault(t *testing.T) {
	_ = os.Setenv(stringKey, "hello")
	val := env.LookupStringWithDefault(stringKey, "goodbye")
	if val != "hello" {
		t.Errorf("env.LookupBool(): got %q, expected %q", val, "hello")
	}
}

func TestLookupStringFindsString(t *testing.T) {
	_ = os.Setenv(stringKey, "something")
	val, err := env.LookupString(stringKey)
	if err != nil {
		t.Errorf("did not expect error")
	}

	if val != "something" {
		t.Errorf("env.LookupString(): got %q, expected %q", val, "something")
	}
}

func TestLookupIntErrKeyNotSet(t *testing.T) {
	_, err := env.LookupInt(intKey)
	if !errors.As(err, &env.ErrKeyNotSet{}) {
		t.Errorf("env.LookupInt(): got err %v, expected err %s", err, "ErrKeyNotSet")
	}
}

func TestLookupIntWithDefaultUsesDefault(t *testing.T) {
	val := env.LookupIntWithDefault(intKey, 79)
	if val != 79 {
		t.Errorf("env.LookupInt(): got %q, expected %q", val, 79)
	}
}

func TestLookupIntWithDefaultDoesNotUseDefault(t *testing.T) {
	_ = os.Setenv(intKey, "34")
	val := env.LookupIntWithDefault(intKey, 10)
	if val != 34 {
		t.Errorf("env.LookupBool(): got %q, expected %q", val, 34)
	}
}

func TestLookupIntFindsInt(t *testing.T) {
	_ = os.Setenv(intKey, "123")
	val, err := env.LookupInt(intKey)
	if err != nil {
		t.Errorf("did not expect error")
	}

	if val != 123 {
		t.Errorf("env.LookupInt(): got %q, expected %q", val, "something")
	}
}

func TestLookupIntErrInvalidType(t *testing.T) {
	_ = os.Setenv(intKey, "true")
	_, err := env.LookupInt(intKey)
	if !errors.Is(err, env.ErrInvalidType) {
		t.Errorf("env.LookupInt(): got err %v, expected err %s", err, "ErrInvalidType")
	}
}

func TestLookupBoolErrKeyNotSet(t *testing.T) {
	_, err := env.LookupBool(boolKey)
	if !errors.As(err, &env.ErrKeyNotSet{}) {
		t.Errorf("env.LookupBool(): got err %v, expected err %s", err, "ErrKeyNotSet")
	}
}

func TestLookupBoolWithDefaultUsesDefault(t *testing.T) {
	val := env.LookupBoolWithDefault(boolKey, true)
	if val != true {
		t.Errorf("env.LookupBool(): got %v, expected %v", val, true)
	}
}

func TestLookupBoolWithDefaultDoesNotUseDefault(t *testing.T) {
	_ = os.Setenv(boolKey, "0")
	val := env.LookupBoolWithDefault(boolKey, true)
	if val != false {
		t.Errorf("env.LookupBool(): got %v, expected %v", val, false)
	}
}

func TestLookupBoolFindsBool(t *testing.T) {
	_ = os.Setenv(boolKey, "1")
	val, err := env.LookupBool(boolKey)
	if err != nil {
		t.Errorf("did not expect error")
	}

	if val != true {
		t.Errorf("env.LookupBool(): got %v, expected %v", val, true)
	}
}

func TestLookupBoolErrInvalidType(t *testing.T) {
	_ = os.Setenv(boolKey, "76")
	_, err := env.LookupBool(boolKey)
	if !errors.Is(err, env.ErrInvalidType) {
		t.Errorf("env.LookupBool(): got err %v, expected err %s", err, "ErrInvalidType")
	}
}
